---
title: "data_get"
output: html_document
editor_options: 
  chunk_output_type: inline
---

```{r}
library(tidyverse)
annot <- read.csv("../data/Annotation/annot.csv") %>% as.tibble()
annot$RFS <- as.numeric(as.character(annot$RFS))
  
  
annot_E1 <- annot %>% filter(DS == "E1")
annot_E1$ER_Status <- ifelse(as.numeric(as.character(annot_E1$ER.Allred)) > 4, 1, 0 )
annot_E1$PR_Status <- ifelse(as.numeric(as.character(annot_E1$PR.Allred)) > 4, 1, 0 )
annot_E1$Response <- ifelse(annot_E1$Response == "Responder", 1, ifelse(annot_E1$Response == "Non-responder", 0, NA))
annot_E1$Biopsy.Time <- ifelse(annot_E1$Biopsy.Time == 1, "TP", ifelse(annot_E1$Biopsy.Time == 2, "T-On", "TS"))


annot_E2 <- annot %>% filter(DS == "E2")  
annot_E2$ER_Status <- ifelse(as.numeric(as.character(annot_E2$ER.Allred)) > 4, 1, 0 )
annot_E2$PR_Status <- ifelse(as.numeric(as.character(annot_E2$PR.Allred)) > 4, 1, 0 )
annot_E2$Response <- ifelse(annot_E2$Response == "R", 1, ifelse(annot_E2$Response == "NR", 0, NA))
annot_E2$Biopsy.Time <- ifelse(annot_E2$Biopsy.Time == 1, "TP", ifelse(annot_E2$Biopsy.Time == 2, "T-On", "TS"))


annot_E3 <- annot %>% filter(DS == "E3")  
annot_E3$ER_Status <- ifelse(as.numeric(as.character(annot_E3$ER.Allred)) > 4, 1, 0 )
annot_E3$PR_Status <- ifelse(as.numeric(as.character(annot_E3$PR.Allred)) > 4, 1, 0 )
annot_E3$Response <- ifelse(annot_E3$Response == "sensitive", 1, 0)
annot_E3$Biopsy.Time <- ifelse(annot_E3$Biopsy.Time == 1, "TP", ifelse(annot_E3$Biopsy.Time == 2, "T-On", "TM"))



annot_C1 <- annot %>% filter(DS == "C1")  
annot_C1$ER_Status <- annot_C1$ER.Allred
annot_C1$ER.Allred <- NA
annot_C1$ER_Status <- ifelse(annot_C1$ER_Status == "Positive", 1, 0)
annot_C1$PR_Status <- ifelse(annot_C1$PR.Allred == "Positive", 1, 0)
annot_C1$RFS <- as.numeric(as.character(annot_C1$RFS))/365
annot_C1$Response <- ifelse(annot_C1$Response == "Complete_responder", 1, 0)
annot_C1$Biopsy.Time <- ifelse(annot_C1$Biopsy.Time == "T1", "TP", 
                               ifelse(annot_C1$Biopsy.Time == "T2", "T-On", 
                                      ifelse(annot_C1$Biopsy.Time == "T3", "TM", "TS")))



annot_C2 <- annot %>% filter(DS == "C2")  
annot_C2$ER_Status <- annot_C2$ER.Allred
annot_C2$ER.Allred <- NA
annot_C2$PR_Status <- NA
annot_C2$RFS <- as.numeric(as.character(annot_C2$RFS))/365
annot_C2$Response <- ifelse(annot_C2$Response == "1", 1, 0)
annot_C2$Biopsy.Time <- ifelse(annot_C2$Biopsy.Time == "T1", "TP", 
                               ifelse(annot_C2$Biopsy.Time == "T2", "T-On", "TS"))

annot2 <- rbind(annot_E1, annot_E2) %>% rbind(annot_E3) %>% rbind(annot_C1) %>% rbind(annot_C2)
annot2$Intrinsic.Subtype <- NULL
annot2$DS <- as.character(annot2$DS)

annot2 %>% dplyr::group_by(DS, Biopsy.Time) %>% dplyr::summarise(x = length(Patient.ID)) %>% arrange(desc(DS))
annot2 %>% dplyr::group_by(DS) %>% dplyr::summarise(x = length(Patient.ID)) %>% arrange(desc(DS))
annot2 %>% dplyr::group_by(Biopsy.Time) %>% dplyr::summarise(x = length(Patient.ID)) %>% mutate(x = sum(x))
```

```{r}
annot2$Study.ID <- 1:1110
annot$Is.ER


annot$Sample.ID
annot$Patient.ID
annot$ER.Allred
annot$PR.Allred
annot$HER2.Status
annot$Response
annot$Nodal.Status
annot$Grade
annot$T
annot$N
annot$M
annot$RFS
annot$Recurrence
annot$Treatment.Type
annot$Biopsy.Time
annot$Platform
annot$Intrinsic.Subtype
annot$DS
annot$Study.ID


#write.csv(annot2, "../data/Annotation/annotation.csv")
```

```{r}
HeatMapAnnot <- read.csv("data/Annotation/annotation.csv")
row.names(HeatMapAnnot) <- HeatMapAnnot$Sample.ID
HeatMapAnnot$Treatment.Type <- ifelse(HeatMapAnnot$Treatment.Type == "NAC", "Chemotherapy", "AI")
VarGenes <- order(abs(pca1$rotation[,1]), decreasing=TRUE)[1:500]
HeatMapIn <- as.data.frame(t(gp6))[VarGenes, ]
HeatMapIn <- HeatMapIn[, which(colnames(HeatMapIn) %in% HeatMapAnnot$Sample.ID)]
HeatMapAnnot <- HeatMapAnnot[which(HeatMapAnnot$Sample.ID %in% colnames(HeatMapIn)), ]

```


```{r, include=FALSE}
source("functions/CreateAnnotationDataFrame_entrez.R")
source("functions/CreateAnnotationDataFrame_hgnc.R")
source("../functions/CreateGeneFuPreds.R")
source("../functions/Create_reference_DF.R")
source("../functions/CreateCombatDF_OnX.R")
source("../functions/CreateGeneFuPreds_hgnc.R")
source("../functions/CreateMergedDFNoNorm.R")
source("../functions/EDA_R.R")
source("../functions/CreateQNMergedDF.R")

bookdown::re

CreateGenefuPreds_all <- function(expressionDF_entrez, expressionDF_hgnc){
  
  expressionDF_entrez <- expressionDF_entrez
  expressionDF_hgnc <- expressionDF_hgnc
  an.dfr_entrez = CreateAnnotationDataframe_entrez(expressionDF_entrez)
  an.dfr_hgnc = CreateAnnotationDataframe_hgnc(expressionDF_hgnc)
  
  mammaprint_risk <- gene70(data = t(expressionDF_entrez), annot = an.dfr_entrez, do.mapping = TRUE)$risk %>%
    as.data.frame()
  rorS_risk <- rorS(data = t(expressionDF_entrez), annot = an.dfr_entrez, do.mapping = TRUE)$risk %>% as.data.frame()
  
  mammaprint_score <- gene70(data = t(expressionDF_entrez), annot = an.dfr_entrez, do.mapping = TRUE)$score %>% as.data.frame()
  rorS_score <- rorS(data = t(expressionDF_entrez), annot = an.dfr_entrez, do.mapping = TRUE)$score %>% as.data.frame()
  
  #scmgene <- molecular.subtyping(
  #  sbt.model = c("scmgene"), 
  #  data = t(expressionDF_entrez), 
  #  annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
  #  as.data.frame()
  
  scmod1 <- molecular.subtyping(
    sbt.model = c("scmod1"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  scmod2 <- molecular.subtyping(
    sbt.model = c("scmod2"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  pam50 <- molecular.subtyping(
    sbt.model = c("pam50"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  ssp2006 <- molecular.subtyping(
    sbt.model = c("ssp2006"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  ssp_2003 <- molecular.subtyping(
    sbt.model = c("ssp2003"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  intClust <- molecular.subtyping(
    sbt.model = c("intClust"), 
    data = t(expressionDF_hgnc), 
    annot = an.dfr_hgnc, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  #AIMS <- molecular.subtyping(
    #sbt.model = c("AIMS"), 
    #data = t(expressionDF_entrez), 
    #annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    #as.data.frame()
  
  subtypes <- cbind(scmod1, scmod2, pam50, ssp2006, ssp_2003, intClust, mammaprint_risk, mammaprint_score, rorS_risk, rorS_score)
  colnames(subtypes) <- c("smcod1", "scmod2", "pam50", "ssp2006", "ssp2003", "IC10", "MammaPrint_risk", "MammaPrint_score", "rorS_risk", "rorS_score")
  subtypes$ID <- row.names(subtypes)
  subtypes
}

```

```{r, include=FALSE}
C1_HGNC <- read.csv("data/C1_DS/C1_HGNC.csv", header = TRUE, row.names = 2) %>% 
  dplyr::select(-"X.1")
C2_HGNC <- read.csv("data/C2_DS/C2_HGNC.csv", header = TRUE, row.names = 1) %>% 
  dplyr::select(-"row.names.merge_names.")
E1_HGNC <- read.csv("data/E1_DS/E1_HGNC.csv", row.names = 1)
E2_HGNC <- read.csv("data/E2_DS/E2_HGNC.csv", row.names = 1)
E3_HGNC <- read.csv("data/E3_DS/E3_HGNC.csv", row.names = 1)

#Loading the expression data with entrez gene names
C1_Entrez <- read.csv("data/C1_DS/C1_entrez.csv", row.names = 1) 
C2_Entrez <- read.csv("data/C2_DS/C2_entrez.csv", row.names = 1) %>% 
  dplyr::select(-"row.names.merge_names.")
E1_Entrez <- read.csv("data/E1_DS/E1_entrez.csv", row.names = 1) %>% 
  dplyr::select(-"row.names.E1.E2.entrez.")
E2_Entrez <- read.csv("data/E2_DS/E2_entrez.csv", row.names = 1)
E3_Entrez <- read.csv("data/E3_DS/E3_entrez.csv", row.names = 1) %>% 
  dplyr::select(-"row.names.merge_names.")



C1 <- CreateGenefuPreds_all(C1_Entrez, C1_HGNC)
C2 <- CreateGenefuPreds_all(C2_Entrez, C2_HGNC)
E1 <- CreateGenefuPreds_all(E1_Entrez, E1_HGNC)
E2 <- CreateGenefuPreds_all(E2_Entrez, E2_HGNC)
E3 <- CreateGenefuPreds_all(E3_Entrez, E3_HGNC)

subtype <- rbind(C1, C2) %>% rbind(E1) %>% rbind(E2) %>% rbind(E3)

annot_update <- merge(HeatMapAnnot, subtype, by = 0)
write.csv(annot_update, "Annotation/annotationST.csv")
```

