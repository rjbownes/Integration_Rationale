---
title: " "
output: html_notebook
---

```{r}

source("../functions/CreateAnnotationDataFrame_entrez.R")
source("../functions/CreateAnnotationDataFrame_hgnc.R")
source("../functions/CreateGeneFuPreds.R")
source("../functions/Create_reference_DF.R")
source("../functions/CreateCombatDF_OnX.R")
source("../functions/CreateGeneFuPreds_hgnc.R")
source("../functions/CreateMergedDFNoNorm.R")
source("../functions/EDA_R.R")
source("../functions/CreateQNMergedDF.R")

library(tidyverse)
library(genefu)
library(preprocessCore)
library(sva)
library(ggthemes)

CreateQNMergedDF <- function(...){
  file_list <- list(...)
  
  l2<-lapply(file_list, function(x) 
  cbind(x, "Symbol" = row.names(x)))
  
  chunk <- l2 %>%
    Reduce(function(dtf1,dtf2) full_join(dtf1,dtf2,by=c("Symbol" = "Symbol")), .)
  
  row.names(chunk) <- chunk$Symbol
  chunk$Symbol <- NULL
  qn_chunk <- normalize.quantiles(as.matrix(chunk)) %>% as.data.frame()
  row.names(qn_chunk) <- row.names(chunk)
  colnames(qn_chunk) <- colnames(chunk)
  qn_chunk
}


#CreateAnnotationDataframe_entrez
#CreateAnnotationDataframe_hgnc
#CreateGenefuPreds
#CreateQNMergedDF
#CreateMergedDF_reference
#CreateCombatDF_OnX

```


```{r}
#Chemo data 1
C1.E <- read.csv("//Users/richard/Documents/Work_files/ajit/other_data/Chemo_DS_1.csv") 
names <- C1.E$X
row.names(C1.E) <- names
C1.E <- C1.E[, -1]
C1.P <- read.csv("//Users/richard/Documents/Work_files/ajit/Chemo_DS_1_pheno.csv")
C1.P$Response <- ifelse(C1.P$Response_status == "Good Response", "Responder", 
                        ifelse(C1.P$Response_status == "Complete_responder", "Responder", "Non Responder"))
colnames(C1.P)[15] <- "Time"


#Chemo data 2
C2.E <- read.csv("//Users/richard/Documents/Work_files/ajit/other_data/Chemo_DS_2.csv")
C2.P <- read.csv("//Users/richard/Documents/Work_files/ajit/other_data/Chemo_DS_2_pheno.csv")
C2.P$Response <- ifelse(C2.P$Response == 0, "Non Responder", "Responder")
C2.P$Sample <- row.names(C2.P) 
C2.E[is.na(C2.E)] <- 0
colnames(C2.P)[2] <- "Patient.Number"
C2.P %>% filter(Time == "T2") %>% dplyr::select(Patient.Number) %>% unique()

#Endocrine Data 1 #Edinburgh
E1.E <- read.csv("//Users/richard/Documents/Work_files/ajit/other_data/Endocrin_1.csv")
E1.P <- read.csv("//Users/richard/Documents/Work_files/ajit/other_data/Endocrin_1_pheno.csv")
E1.P$Response <- ifelse(E1.P$response == "Responsive", "Responder", 
                        ifelse(E1.P$response == "Partial", "Responder", "Non Responder")) 
colnames(E1.P)[2] <- "Sample"
colnames(E1.P)[3] <- "Time"



#Endocrine Data 2 #Eliis
E2.E <- read.csv("//Users/richard/Documents/Work_files/ajit/other_data/Endocrine_2.csv")
E2.E[is.na(E2.E)] <- 0
E2.P <- read.csv("//Users/richard/Documents/Work_files/ajit/other_data/Endocrine_2_pheno.csv")
E2.P$Response <- ifelse(E2.P$response == "resistant", "Non Responder", "Responder")
E2.P$Sample <- E2.P$GSM
colnames(E2.P)[5] <- "Time"
E2.P$patient <- gsub(pattern = "_BL|_W2|_M|-BL", replacement = "", x = E2.P$patient, ignore.case = TRUE)

#Endocrine Data 3 #Marsden
E3.E <- read.csv("//Users/richard/Documents/Work_files/ajit/other_data/Endocrine_3.csv")
library(plyr)
E3.E <- ddply(E3.E, "X", numcolwise(mean))
names <- E3.E$X
row.names(E3.E) <- names
E3.E$X <- NULL
E3.P <- read.csv("//Users/richard/Documents/Work_files/ajit/other_data/Endo_3_pheno.csv")
E3.P$Response <- ifelse(E3.P$V2 == "responder", "Responder", "Non Responder")
colnames(E3.P)[2] <- "Sample"
colnames(E3.P)[4] <- "Time"
#install.packages("parallel")

E3.E["ESR1", ]
``` 

```{r}
gp1 <- CreateQNMergedDF(E3.E, C2.E)
gp2 <- CreateQNMergedDF(C1.E, C2.E, C1.E)
gp3 <- CreateQNMergedDF(C1.E, C2.E, C1.E, E1.E)
gp4 <- CreateQNMergedDF(C1.E, C2.E, E3.E, E1.E, E2.E)
gp5 <- CreateQNMergedDF(E1.E, E2.E, E3.E, C1.E, C2.E)

gp5 %>% 
  mutate(genes = row.names(.)) %>% 
  gather(key = key, value = value, -genes) %>% 
  mutate(DS = rep(c(1,2,3,4,5), c(nrow(gp5)*ncol(E1.E),nrow(gp5)*ncol(E2.E),nrow(gp5)*ncol(E3.E),nrow(gp5)*ncol(C1.E),nrow(gp5)*ncol(C2.E)))) %>% 
  ggplot(aes(value, color = as.factor(DS))) + 
  geom_density()


f1 <- function(vec) {
        m <- mean(vec, na.rm = TRUE)
        vec[is.na(vec)] <- m
        return(vec)
}


gp6 <- apply(t(gp5), 2, f1)

pca1 <- prcomp(gp6)

pca2 <- pca1$x %>% as_tibble() %>% dplyr::select(PC1, PC2) %>% mutate(DS = rep(c("Edinburgh", "Baylor", "Marsden", "NEO", "I-SPY"), c(length(colnames(E1.E)),
                                                                                                                                      length(colnames(E2.E)),
                                                                                                                                      length(colnames(E3.E)),
                                                                                                                                      length(colnames(C1.E)),
                                                                                                                                      length(colnames(C2.E)))))
pca3 <- pca2 %>% ggplot(aes(x = PC1, y = PC2, color = DS)) + geom_point()



data1 <- tibble("Possible_Gene_Count" = c(nrow(E3.E), 
                        nrow(C2.E),
                        nrow(C1.E), 
                        nrow(E1.E), 
                        nrow(E2.E)), 
       "Actual_Gene_Count" = c(nrow(E3.E),
                        nrow(gp2), 
                        nrow(gp3), 
                        nrow(gp1),
                        nrow(gp4)), 
       "X" = c(1,2,3,4,5))

ggplot(data = data1) + 
  geom_line(mapping = aes(x = X, y = Possible_Gene_Count, color = "Possible Gene Count")) + 
  geom_line(mapping = aes(x = X, y = Actual_Gene_Count, color = "Actual Gene Count")) + 
  theme_pander() + 
  xlab("Number of datasets") +
  ylab("Number of Genes") +
  geom_hline(yintercept = 6000) +
  geom_vline(xintercept = 4)
```

```{r}
names <- c("Sample", "Time", "Ds")
p1 <- E1.P[, c(2, 3)]
p1$DS <- 1
p2 <- E2.P[, c(2, 5)]
p2$DS <- 2
p3 <- E3.P[, c(2, 4)]
p3$DS <- 3
p4 <- C1.P[, c(2, 15)]
p4$DS <- 4
p5 <- C2.P[, c(6, 1)]
p5$DS <- 5

dtw <- list(p1,p2,p3,p4,p5)

funx <- function(x){
  colnames(x) <- names
  x
}

ret <- lapply(X = dtw, FUN = funx)

bin <- do.call(what = rbind, args = ret)

bin2 <- merge(bin, pre_5, by = "ID")
row.names(bin2) <- bin2$ID

dtw2 <- CreateCombatDF_OnX(gp5, bin2[, c(1,7)])

row.names(bin) <- bin$Sample

pca1 <- prcomp(t(dtw2))$x
pca1 <- pca1 %>% as.data.frame()
pca1 <- pca1[, c(1,2)]
pcao <- merge(bin2, as.data.frame(pca1)[, c(1,2)], by = 0)
c("Edinburgh", "Baylor", "Marsden", "NEO", "I-SPY")
pcao$DS <- ifelse(pcao$Ds == "1", "Edinbugh", 
                  ifelse(pcao$Ds == "2", "Baylor", 
                         ifelse(pcao$Ds == "3", "Marsden", 
                                ifelse(pcao$Ds == "4", "NEO", "I-SPY"))))
row.names(pcao) <- pcao$Row.names
ggplot(as.data.frame(pcao %>% filter(Time != "T4")), aes(PC1, PC2, color = DS)) + geom_point() 
```

```{r}
library(pheatmap)

gps5 <- as.data.frame(t(scale(t(dtw2))))
pheatmap(gps5, cluster_rows = TRUE, annotation_col = pcao[,c(4,5,8)], show_rownames = FALSE,show_colnames = FALSE, )

aed <- list(E1.E, E2.E, E3.E, C1.E, C2.E)
aed1 <- list(E1.E, E2.E)
aed2 <- list(E1.E, E3.E, C1.E, C2.E)
aed2 <- list(E2.E)


E2.E2 <- (E2.E)^2

aed <- list(E1.E, E3.E, C1.E, C2.E)
source("//Users/richard/CuratedMatchedBreast/functions/CreateGeneFuPreds_hgnc.R")
library(transcripTools)

E1.EE <- idReplace(E1.E, format_in = "hgnc_symbol", "entrezgene")
E2.EE <- idReplace(E2.E, format_in = "hgnc_symbol", "entrezgene")
E3.EE <- idReplace(E3.E, format_in = "hgnc_symbol", "entrezgene")
C1.EE <- idReplace(C1.E, format_in = "hgnc_symbol", "entrezgene")
C2.EE <- idReplace(C2.E, format_in = "hgnc_symbol", "entrezgene")

ee.5_QN_DS <- idReplace(input = dtw2, format_in = "hgnc_symbol", format_out = "entrezgene")

pre1 <- test_funn(E1.EE, E1.E)
pre2 <- test_funn(E2.EE, E2.E)
pre3 <- test_funn(E3.EE, E3.E)
pre4 <- test_funn(C1.EE, C1.E)
pre5 <- test_funn(C2.EE, C2.E)

pre_list <- list(pre1, pre2, pre3, pre4, pre5)

pre_5 <- do.call(rbind, pre_list)



post5 <- test_funn(ee.5_QN_DS, dtw2)

comp1 <- merge(pre_5 %>% gather(key = key, value = value, -ID, -MammaPrint_score), post5 %>% gather(key = key, value = value, -ID, -MammaPrint_score), by = "ID")
bin$ID <- bin$Sample

comp2 <- merge(comp1, bin, by = "ID")

comp2$test <- comp2$value.x == comp2$value.y

comp2 <- comp2 %>% dplyr::group_by(Time) %>% dplyr::mutate(test2 = mean(test))

comp2 %>% 
  filter(Time != "T4") %>% 
  ggplot(aes(x = Time, y = test2)) +
    geom_point()

comp2 %>% 
  filter(Time != "T4") %>% 
  ggplot(aes(x = MammaPrint_score.x, y = MammaPrint_score.y)) +
  geom_point() +
  geom_smooth() +
  geom_smooth(mapping = aes(x = MammaPrint_score.x, y = MammaPrint_score.x))



library(ggpubr)
gp52 <- gp5[, colnames(dtw2)]



cor_mat <- cor(dtw2, gp52) %>% 
  diag() %>% 
  as.data.frame() %>% 
  mutate(Sample = colnames(dtw2)) %>% 
  merge(bin, by = "Sample") %>% 
  filter(Time != "T4")

cor_mat$Time <- as.character(cor_mat$Time)

test <- list(c("T1", "T2"))

ggplot(cor_mat, aes(x = Time, y = .)) + 
  geom_boxplot() + 
  geom_jitter(aes(color = as.factor(Ds))) + 
  facet_wrap(~Ds) +
  stat_compare_means(method = "wilcox.test", label = "p.signif", comparisons = test)


```

```{r}
test_funn <- function(x, y){
  expressionDF_entrez <- x
  an.dfr_entrez <- CreateAnnotationDataframe_entrez(expressionDF = x)
  
  exp.hgnc <- y
  an.dfr_hgnc <- CreateAnnotationDataframe_hgnc(y)
  
  
  mammaprint_risk <- gene70(data = t(expressionDF_entrez), annot = an.dfr_entrez, do.mapping = TRUE)$risk %>% as.data.frame()
  #rorS_risk <- rorS(data = t(expressionDF_entrez), annot = an.dfr_entrez, do.mapping = TRUE)$risk %>% as.data.frame()
  
  mammaprint_score <- gene70(data = t(expressionDF_entrez), annot = an.dfr_entrez, do.mapping = TRUE)$score %>% as.data.frame()
  #rorS_score <- rorS(data = t(expressionDF_entrez), annot = an.dfr_entrez, do.mapping = TRUE)$score %>% as.data.frame()
  
  
  scmod1 <- molecular.subtyping(
    sbt.model = c("scmod1"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  scmod2 <- molecular.subtyping(
    sbt.model = c("scmod2"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  pam50 <- molecular.subtyping(
    sbt.model = c("pam50"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  ssp2006 <- molecular.subtyping(
    sbt.model = c("ssp2006"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  ssp_2003 <- molecular.subtyping(
    sbt.model = c("ssp2003"), 
    data = t(expressionDF_entrez), 
    annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  intClust <- molecular.subtyping(
    sbt.model = c("intClust"), 
    data = t(exp.hgnc), 
    annot = an.dfr_hgnc, do.mapping = TRUE)$subtype %>% 
    as.data.frame()
  
  #AIMS <- molecular.subtyping(
    #sbt.model = c("AIMS"), 
    #data = t(expressionDF_entrez), 
    #annot = an.dfr_entrez, do.mapping = TRUE)$subtype %>% 
    #as.data.frame()
  
  subtypes <- cbind(scmod1, scmod2, pam50, ssp2006, ssp_2003, intClust, mammaprint_risk, mammaprint_score)
  colnames(subtypes) <- c("smcod1", "scmod2", "pam50", "ssp2006", "ssp2003", "IC10", "MammaPrint_risk", "MammaPrint_score")
  subtypes$ID <- row.names(subtypes)
  subtypes
}
```
