# Alternative

Perhaps it is the size of the data, or complexities of covariates and features out of our control, but integration en masse of these different studies appears to do more harm than good, and results gleaned from the resulting merged dataset would have to be treated **very** carefully. This has not yet even taken in to consideration the true value of these cohorts which are the across time samples. Relative changes in these samples within a patient across treatment time are what make these studies extra valuable but will be massively confounded by the illustrated factors. In order to circumvent this I am proposing aggregating the data in a standard S4 datatype, the ExpressionSetList, with carefully crafted phenodata and feature data in order to perform specific analysis of this data across the datasets using bivariate entries in the annotation. In this way, the data can be minimally disturbed as possible, but the relative changes, results of analysis and differential expression can be compared. Below is an example of what the C1 entry in this list would look like.

```{r, include=FALSE}
library(XDE)
library(tidyverse)
```

```{r}
data <- attach("data/eset_list.rda")
data$eset_list[[1]]
```

Here we see the expression values for each sample. 

```{r}
C1 <- data$eset_list[[1]]
exprs(C1) %>% as_tibble()
```

The feature data captures the rownames of each expression set and are stored as an extra object. 

```{r}
fData(C1)
```

And lastly the study relevant information can be callsed with the standard Biobase::pData() function. 

```{r}
pData(C1) %>% as_tibble()
```


