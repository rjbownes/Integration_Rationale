ExploratoryGraphicalAnalysis <- function(v,w,x,y,z){
  
  Untreated <- right_join(z, x, by = "ID") %>% 
    gather(key=key, value=value, -ID, -Time, -DS, -MammaPrint_score, -MammaPrint_risk, -rorS_score, -rorS_risk)
  
  treated <- right_join(z, y, by = "ID") %>% 
    gather(key=key, value=value, -ID, -Time, -DS, -MammaPrint_score, -MammaPrint_risk, -rorS_score, -rorS_risk)
  
  treated <- treated[which(treated$ID %in% Untreated$ID), ]
  
  Combined <- cbind(Untreated, treated)
  
  colnames(Combined) <- paste0(colnames(Combined), rep(c(1,2), c(length(colnames(Combined))/2, length(colnames(Combined))/2)))
  
  a <- ggplot(Combined, aes(x = MammaPrint_score2, y = MammaPrint_score1, color = as.factor(MammaPrint_risk1))) + 
    geom_point() + 
    facet_grid(Time1~DS1) + 
    geom_smooth(method = "loess") + 
    labs(color = "Risk") + 
    geom_vline(xintercept = -0.3)
  
  b <- ggplot(Combined, aes(x = rorS_score2, y = rorS_score1, color = as.factor(rorS_risk1))) + 
    geom_point() + 
    facet_grid(Time1~DS1) + 
    geom_smooth(method = "loess") + 
    labs(color = "Risk") + 
    geom_vline(xintercept = c(40, 60))
  
  c <- ggplot(Combined %>% 
                dplyr::select(DS1, Time1, MammaPrint_risk1, MammaPrint_risk2, MammaPrint_score1, MammaPrint_score2) %>% 
                gather(key = key, value = value, -DS1, -Time1, -MammaPrint_risk2, -MammaPrint_risk1), aes(x = key, y = value, alpha = 0.9)) + 
    geom_jitter(width = 0.1) +
    geom_boxplot() +
    facet_grid(DS1~Time1, scales = "free") + 
    stat_compare_means(method = "t.test")
  
  Inp1 <- v[, which(colnames(v) %in% colnames(w))]
  Inp2 <- w[, which(colnames(w) %in% colnames(v))]
  
  cor.map <- as.data.frame(cor(Inp1, Inp2))
  
  ids_cor <- colnames(cor.map)
  
  ord <- all_pheno[which(z$ID %in% ids_cor), ]
  ord2 <- ord[match(ids_cor, ord$ID), ]
  
  row.names(ord2) <-  ord2$ID
  d <- pheatmap(cor.map, annotation_col = ord2, cluster_rows = TRUE)
  
  output_list <- list(a, b, c, d)
  
  output_list
}