# Treatment Status

Correction for the batch alone was insufficent for combining the studies in a non-detrimental way, next I will explore the effects of adding a covariate for the treatment status of each sample (Pre-treatment, early on-treatment, late on-treatment and resection sample). The hypothesis being that the changes in gene expression caused by the treatment, either aromatase inhibtors or chemotherapy, and the length of time on that treatment are causing significant changes that must be accounted for during integration in order to preserve the biological identity of each sample, but still be able to appropriatly pool the data. 

```{r, include=FALSE, eval=FALSE}
annotation <- read.csv("data/Annotation/annotationST.csv", row.names = 2) %>% dplyr::select(-"X.1")

integration_fun_amended <- function(DF_inp, annot){
  annot <- annot[which(annot[,2] != "NA"), ]
  DF_inp <- DF_inp[, names(DF_inp) %in% annot[,1]]
  annot <- annot[which(annot[,1] %in% names(DF_inp)), ]
  x <- c(seq(levels(as.factor(as.character(annot[,3])))))[annot[,3]]
  y <- model.matrix(~annot[, 2], data=as.factor(colnames(DF_inp)))
  z <- ComBat(dat = as.matrix(na.omit(DF_inp)), batch = x, mod = y, par.prior = TRUE, prior.plots = FALSE) %>% as.data.frame()
  z
}

```

Below is the kernel density distributed data for the five datasets, as with batch correction, it is reassuring that some of the variation in gene expression has returned, but that the distributions are similiar in size, magnitude and are significantly overlapping. 

```{r, include=FALSE, eval=FALSE}
gp5_for_int <- gp5 %>% mutate_all(~ifelse(is.na(.x), mean(.x, na.rm = TRUE), .x))
row.names(gp5_for_int) <- row.names(gp5)


Treatment_Integrated5 <- integration_fun_amended(gp5_for_int, annotation[, c(2, 16, 18)])

TT5DS_Int <- Treatment_Integrated5 %>% 
  mutate(genes = row.names(.)) %>% 
  gather(key = key, value = value, -genes) %>% 
  mutate(DS = rep(c(1,2,3,4,5),
                  c(nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "C1")),
                    nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "C2")),
                    nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "E3")),
                    nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "E1")),
                    nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "E2"))))) %>% 
  na.omit %>% 
  ggplot(aes(value, color = as.factor(DS))) + 
  geom_density() +
  xlim(-50,100)
ggsave("conserved_figures/03/TT5DS_Int.png")
```

![](conserved_figures/03/TT5DS_Int.png)

PCA diagram of this date below shows a similar pattern of overlap between E1 and C2, but now four of the five datasets are very clearly fracturing. Once again these groups are not explainable by any readily available clinical or phenotypic information. 

```{r, include=FALSE, eval=FALSE}
f1 <- function(vec) {
        m <- mean(vec, na.rm = TRUE)
        vec[is.na(vec)] <- m
        return(vec)
}


PADS5_TT <- apply(Treatment_Integrated5, 1, f1) %>% t() %>% as.data.frame()

PADS5_PCA1_TT <- prcomp(t(PADS5_TT))

PADS5_pca2_TT <- PADS5_PCA1_TT$x %>% 
  as_tibble() %>% 
  dplyr::select("PC1", "PC2") %>% 
  mutate(DS = rep(c("Neo", "I-Spy", "Baylor", "Edinburgh", "Marsden"),
                  c(nrow(annotation %>% filter(DS == "C1")), 
                  nrow(annotation %>% filter(DS == "C2")),
                  nrow(annotation %>% filter(DS == "E3")),
                  nrow(annotation %>% filter(DS == "E1")),
                  nrow(annotation %>% filter(DS == "E2")))))

Pca_UPI_TT <- PADS5_pca2_TT %>% 
  ggplot(aes(x = PC1, y = PC2, color = DS)) + 
  geom_point() + 
  scale_color_manual(values = c("red", "blue", "green", "pink", "black"))
ggsave("conserved_figures/03/Pca_UPI_TT.png", Pca_UPI_TT)
```

![](conserved_figures/03/Pca_UPI_TT.png)

The heatmap of the 500 most variable genes shows the same distribution of datasets, with annotation for trial described response, treatment type, and treatement status. 

```{r, include=FALSE, eval=FALSE}

VarGenes <- order(abs(PADS5_PCA1_TT$rotation[,1]), decreasing=TRUE)[1:500]
HeatMapIn <- Treatment_Integrated5[VarGenes, ]
HeatMapIn <- HeatMapIn[, which(colnames(HeatMapIn) %in% annotation$Sample.ID)]

range02 <- function(x){ 
  (x - min(x))/(max(x)-min(x)) * -2 + 1 
}

ggg_TT <- apply(HeatMapIn, 1, range02) %>% t() %>% as.data.frame() 

colnames(ggg_TT) <- colnames(HeatMapIn)

tester_TT <- pheatmap(ggg_TT, annotation_col = annotation[, c(7, 15, 16, 18)], cluster_rows = TRUE, cluster_cols = TRUE, annotation_names_row = FALSE, annotation_names_col = FALSE, show_rownames = FALSE, show_colnames = FALSE, scale = "none", filename = "conserved_figures/Heatmap03.png")
```

![Heat map of the 500 most variable genes colored with annotation of dataset, biopsy time and treatment type.](conserved_figures/Heatmap03.png)


```{r, include=FALSE, eval=FALSE}
source("functions/CreateAnnotationDataFrame_entrez.R")
source("functions/CreateAnnotationDataFrame_hgnc.R")
source("functions/CreateGeneFuPreds_2DS.R")

gene_fu_DS_int <- Treatment_Integrated5 %>% as.data.frame()



C1_DS_INT_HGNC <- gene_fu_DS_int[, colnames(C1_Entrez)]
C2_DS_INT_HGNC <- gene_fu_DS_int[, colnames(C2_Entrez)]
E1_DS_INT_HGNC <- gene_fu_DS_int[, which(colnames(gene_fu_DS_int) %in% colnames(E1_Entrez))]
E2_DS_INT_HGNC <- gene_fu_DS_int[, colnames(E2_Entrez)]
E3_DS_INT_HGNC <- gene_fu_DS_int[, colnames(E3_Entrez)]

C1_DS_INT_Entrez <- read.csv("data/C1_DS/TI_hgnc.csv", row.names = "X")
C2_DS_INT_Entrez <- read.csv("data/C2_DS/TI_hgnc.csv", row.names = "X")
E1_DS_INT_Entrez <- read.csv("data/E1_DS/TI_hgnc.csv", row.names = "X")
E2_DS_INT_Entrez <- read.csv("data/E2_DS/TI_hgnc.csv", row.names = "X")
E3_DS_INT_Entrez <- read.csv("data/E3_DS/TI_hgnc.csv", row.names = "X")

C1_genefupreds <- test_fun(C1_DS_INT_Entrez, C1_DS_INT_HGNC)
C2_genefupreds <- test_fun(C2_DS_INT_Entrez, C2_DS_INT_HGNC)
E1_genefupreds <- test_fun(E1_DS_INT_Entrez, E1_DS_INT_HGNC)
E2_genefupreds <- test_fun(E2_DS_INT_Entrez, E2_DS_INT_HGNC)
E3_genefupreds <- test_fun(E3_DS_INT_Entrez, E3_DS_INT_HGNC)

BC_Subtype_distortion <- list(C1_genefupreds, C2_genefupreds, E1_genefupreds, E2_genefupreds, E3_genefupreds)
BC_Subtype_distortion <- do.call(rbind, BC_Subtype_distortion)
BC_Subtype_distortion$Sample.ID <-  row.names(BC_Subtype_distortion)

```

In replicating this diagram from the first chapter, similar discordance is observed between the pre/post integrated samples, regardless of time or test. 

```{r, eval=FALSE}
(
ST_by_time_change <- annotation %>% 
  dplyr::select(Sample.ID, Patient.ID, Treatment.Type, Biopsy.Time, DS, smcod1, scmod2, pam50, ssp2006, ssp2003, IC10, MammaPrint_risk, rorS_risk) %>% 
  left_join(., BC_Subtype_distortion %>% dplyr::select(-MammaPrint_score, -rorS_score), by = "Sample.ID") %>%
  gather(key = key, value = value, -Sample.ID, -Patient.ID, -Treatment.Type, -Biopsy.Time, -DS, -ID) %>%
  mutate(key = gsub("\\.[xy]", "", .$key)) %>% 
  mutate(Int_status = rep(c(0,1), c(8792, 8792))) %>% 
  #filter(Biopsy.Time == "TP") %>% 
  ggplot(aes(x = Patient.ID, y = as.factor(Int_status), fill = value)) + 
  geom_tile() +
  facet_grid(Biopsy.Time~key) +
  scale_fill_manual(values = c("green","red","red","blue","red","green","pink","pink","red","darkorange3","slateblue4", "darkseagreen","deeppink4","azure3","darkorange4","khaki1","dodgerblue4","goldenrod3","plum3","yellow","green","darkblue","lightblue","green")) + 
  coord_polar() + 
  theme_pander() +
  theme(legend.position = "none", axis.text = element_blank(), axis.ticks = element_blank()) 
)
```

![](conserved_figures/03/ST_by_time_change.png)

Here is the close up of the pre-treatment Pam50 subtypes again to illustrate just one example more closely. Again, while a specific intrinsic subtype is most likely to stay the same, a large portion of samples are categorically different, and there is not a perceivable trend. 

```{r, eval=FALSE}
DF_order <- annotation %>% 
  dplyr::select(Sample.ID, Patient.ID, Treatment.Type, Biopsy.Time, DS, smcod1, scmod2, pam50, ssp2006, ssp2003, IC10, MammaPrint_risk, rorS_risk) %>% 
  left_join(., BC_Subtype_distortion %>% dplyr::select(-MammaPrint_score, -rorS_score), by = "Sample.ID") %>%
  gather(key = key, value = value, -Sample.ID, -Patient.ID, -Treatment.Type, -Biopsy.Time, -DS, -ID) %>%
  mutate(key = gsub("\\.[xy]", "", .$key)) %>% 
  mutate(Int_status = rep(c(0,1), c(8792, 8792))) %>%
  filter(Biopsy.Time == "TP" & key == "pam50" & Int_status == 0) %>% 
  unique() %>% 
  arrange(value) %>% 
  dplyr::select(Patient.ID)


(
pam_50_t1 <- annotation %>% 
  dplyr::select(Sample.ID, Patient.ID, Treatment.Type, Biopsy.Time, DS, smcod1, scmod2, pam50, ssp2006, ssp2003, IC10, MammaPrint_risk, rorS_risk) %>% 
  left_join(., BC_Subtype_distortion %>% dplyr::select(-MammaPrint_score, -rorS_score), by = "Sample.ID") %>%
  gather(key = key, value = value, -Sample.ID, -Patient.ID, -Treatment.Type, -Biopsy.Time, -DS, -ID) %>%
  mutate(key = gsub("\\.[xy]", "", .$key)) %>% 
  mutate(Int_status = rep(c(0,1), c(8792, 8792))) %>%
  filter(Biopsy.Time == "TP" & key == "pam50" & Patient.ID %in% DF_order$Patient.ID) %>%
  ggplot(aes(x = Patient.ID, y = as.factor(Int_status), fill = value)) + 
  geom_tile() +
  scale_x_discrete(limits = rev(DF_order$Patient.ID)) +
  scale_fill_manual(values = c("red", "pink", "Darkblue", "lightblue", "green"))
)
ggsave("conserved_figures/03/pam_50_t1.png")
```

![](conserved_figures/03/pam_50_t1.png)

Mammaprint scoring tells the same story, samples are signficantly (according to ninety five percent confidence intervals) different post integration when compared to pre. 

```{r, eval=FALSE}
(
mammaprint_difference <- annotation %>% 
  dplyr::select(Sample.ID, Patient.ID, Treatment.Type, Biopsy.Time, DS, MammaPrint_score, rorS_score) %>% 
  left_join(., BC_Subtype_distortion %>% dplyr::select(Sample.ID, MammaPrint_score, rorS_score), by = "Sample.ID") %>%
  ggplot(aes(x = MammaPrint_score.x, y = MammaPrint_score.y)) + 
  geom_point(mapping = aes(color = DS)) + 
  geom_smooth(mapping = aes(color = DS)) +
  geom_point(data = reference, aes(x = reference$x, y = reference$y)) +
  xlim(-.89, .75) +
  ylim(-0.6, 0.6)
)
```

![](conserved_figures/03/mammaprint_difference.png)

This result is less pronounced in the rorS risk value, but is still present. 

```{r, eval=FALSE}
(
rorS_difference <- annotation %>% 
  dplyr::select(Sample.ID, Patient.ID, Treatment.Type, Biopsy.Time, DS, MammaPrint_score, rorS_score) %>% 
  left_join(., BC_Subtype_distortion %>% dplyr::select(Sample.ID, MammaPrint_score, rorS_score), by = "Sample.ID") %>%
  ggplot(aes(x = rorS_score.x, y = rorS_score.y)) + 
  geom_point(mapping = aes(color = DS)) + 
  geom_smooth(mapping = aes(color = DS)) +
  geom_point(data = reference2, aes(x = reference2$x, y = reference2$y))
)
ggsave("conserved_figures/03/rorS_difference.png")
```

![](conserved_figures/03/rorS_difference.png)
