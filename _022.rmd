

# Platform integration

The most logical starting place for dataset integration is to integrate based on the cohort. These are all fundamentally different experiments, with different staff, institutes, machinery, collection methods, processing platforms, year of collection, there are even factors so benign, like elevation that could in some way make a difference that are inevitably going to differ. A factor that by default encompasses many of these elements is the "dataset" as it is a resonable assumption that two samples collected for one cohort are collected under more similiar external conditions than two samples from different datasets. 

```{r, include=FALSE}

annotation <- read.csv("data/Annotation/annotationST.csv", row.names = 2) %>% dplyr::select(-"X.1")

integration_fun <- function(DF_inp, annot){
  annot <- annot[which(annot[,2] != "NA"), ]
  DF_inp <- DF_inp[, names(DF_inp) %in% annot[,1]]
  annot <- annot[which(annot[,1] %in% names(DF_inp)), ]
  x <- c(seq(levels(as.factor(as.character(annot[,2])))))[annot[,2]]
  y <- model.matrix(~1, data=as.factor(colnames(DF_inp)))
  z <- ComBat(dat = as.matrix(na.omit(DF_inp)), batch = x, mod = y, par.prior = TRUE, prior.plots = FALSE) %>% as.data.frame()
  z
}
```

```{r}
gp5_for_int <- gp5

gp5_for_int[] <- lapply(gp5_for_int, function(x) { 
  x[is.na(x)] <- mean(x, na.rm = TRUE)
  x
})


DSIntegrated5 <- integration_fun(gp5_for_int, annotation[, c(2, 18)])

#C1_HGNC
#C2_HGNC
#E1_HGNC
#E2_HGNC
#E3_HGNC
#
#C1_Entrez
#C2_Entrez
#E1_Entrez
#E2_Entrez
#E3_Entrez

#DSIntegrated5_dist <- apply(DSIntegrated5, 1, scale) %>% as.data.frame()

DataDist5DS_Int <- DSIntegrated5 %>% 
  mutate(genes = row.names(.)) %>% 
  gather(key = key, value = value, -genes) %>% 
  mutate(DS = rep(c(1,2,3,4,5),
                  c(nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "E1")),
                    nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "E2")),
                    nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "E3")),
                    nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "C1")),
                    nrow(DSIntegrated5)*nrow(annotation %>% filter(DS == "C2"))))) %>% 
  na.omit %>% 
  ggplot(aes(value, color = as.factor(DS))) + 
  geom_density() +
  xlim(-50,100)

```

```{r}
DataDist5DS_Int
```

```{r, include=FALSE}
f1 <- function(vec) {
        m <- mean(vec, na.rm = TRUE)
        vec[is.na(vec)] <- m
        return(vec)
}


PADS5 <- apply(DSIntegrated5, 1, f1) %>% t() %>% as.data.frame()

PADS5_PCA1 <- prcomp(t(PADS5))
#colnames(DSIntegrated5)

PADS5_pca2 <- PADS5_PCA1$x %>% 
  as_tibble() %>% 
  dplyr::select("PC1", "PC2") %>% 
  mutate(DS = rep(c("Neo", "I-Spy", "Baylor", "Edinburgh", "Marsden"),
                  c(nrow(annotation %>% filter(DS == "C1")), 
                  nrow(annotation %>% filter(DS == "C2")),
                  nrow(annotation %>% filter(DS == "E3")),
                  nrow(annotation %>% filter(DS == "E1")),
                  nrow(annotation %>% filter(DS == "E2")))))

Pca_UPI <- PADS5_pca2 %>% 
  ggplot(aes(x = PC1, y = PC2, color = DS)) + 
  geom_point() + 
  scale_color_manual(values = c("red", "blue", "green", "pink", "black"))

```

```{r}
Pca_UPI
```

```{r, include=FALSE}
annotation
VarGenes <- order(abs(PADS5_PCA1$rotation[,1]), decreasing=TRUE)[1:500]
HeatMapIn <- DSIntegrated5[VarGenes, ]
HeatMapIn <- HeatMapIn[, which(colnames(HeatMapIn) %in% annotation$Sample.ID)]

center_scale <- function(x) {
  scale(x, center = TRUE, scale = FALSE)
}

range02 <- function(x){ 
  (x - min(x))/(max(x)-min(x)) * -2 + 1 
}

ggg <- apply(HeatMapIn, 1, range02) %>% t() %>% as.data.frame() 

colnames(ggg) <- colnames(HeatMapIn)

tester <- pheatmap(ggg, annotation_col = annotation[, c(7, 15, 16, 18)], cluster_rows = TRUE, cluster_cols = TRUE, annotation_names_row = FALSE, annotation_names_col = FALSE, show_rownames = FALSE, show_colnames = FALSE, scale = "none", filename = "conserved_figures/Heatmap02.png")
```

![Heat map of the 500 most variable genes colored with annotation of dataset, biopsy time and treatment type.](conserved_figures/Heatmap02.png)

