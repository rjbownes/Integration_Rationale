# Integration of sequentially sampled breast cancer data sets to improve statistical testing and sample size.  

## Abstract

Background: Neoadjuvant therapy represents a unique opportunity to monitor
tumour expression level changes and response to treatment in close to real
time. Changes in the transcriptomic landscape of cancer on-treatment have
already been shown to correlate with response and outcome in labeled patient
data from modestly sized trials. By combining several contemporary studies,
more significant findings can be reached to further the goal of personalized
medicine.

Methods: Five transcriptomic datasets of primary breast cancer were collected
and combined using *ComBat* to create a unified expression dataset with matched
annotation data. Results of each method were compared with unintegrated and
separately analyzed data to establish discordance, and to parallel methods to
evaluate performance. 

Results: Integration of pre and on-treatment samples proved remarkably complex,
and attempts with existing techniques exposed new challenges to large scale
integration. When taking into account important biological and systematic
differences between the datasets as factors for integration, this presented
an exponentially expanding list of possibilities to consider for the fair and
rational integration of datasets, and for the given subset of covariates
utilized, integration of these datasets failed to return concordant values for
expression, intrinsic subtype or calculated risk scores.

** Unsupervised methods of clustering clearly indicated the data was incompletely
integrated, and most of the variance present in the dataset was still
represented by the original batch. Other methods were explored to circumvent
these issues which led to the creation of a new publicly available dataset for
the meta-analysis of these resultant data. (Need %, pvalues, etc). **

Conclusion: This study evaluated methodologies for the integration of disparate
data sets to create the largest uniformly annotated transcriptional dataset of
sequentially patient matched neo-adjuvant primary breast cancer. However,
unifying transcriptional data across multiple studies proved an insurmountable
task to accomplish and maintain the underlying biological variation. In place
of a single uniform transcriptional dataset, an expression list object of
similarly normalized data, with unified annotation data for analysis is
presented to provide opportunities for on-treatment biomarker identification
and validation.  


## Background

** __ consider moving to introduction__ Primary breast cancer is the most commonly diagnosed cancer in
women[@bray2018global][@Hayes_2015]. In 2012 there were 1.7 million newly
diagnosed cases, and 500,000 deaths from BC, this represents almost a quarter
of all newly diagnosed cancers, and 15% of deaths from cancer in women
[@Torre_2015]. These numbers represent a heterogeneous disease of different
clinical, histological and intrinsic factors [@Polyak_2011]. Additionally, in
supervised analysis of patients with known clinical and prognostic outcomes,
inter patient variation often exceeds the variance between groups
[@Stephens_2012]. This means that identifying trends that significantly
differentiate translational changes between responsive and non-responsive
breast cancer can be very difficult, and is heavily confounded by sample size.
Study sample size is then of utmost importance for response prediction and
classification of patients as underpowered trials will yield insignificant
results.

Datasets of well annotated clinical trial data have been created such as the
NKI [@van_t_Veer_2002] and efforts to create large meta datasets have resulted
in projects like METABRIC [@Curtis_2012] and the breast relevant breast cancer
samples in the TCGA [@2012]. These are all comprised of pre-treatment biopsy
samples of patients treated with standard of care adjuvant therapies. These
have resulted in several novel new approaches for breast cancer subtyping and
risk assesment (70 gene signature, IC10), including FDA approval and expansive
clinical trials [@Cardoso_2016]. Conditionally, these have been successful due
the large and well annotated nature of their training data.

Neoadjuvant therapy for primary breast cancer is a growing area of treatment
that is showing significant results for patient care and outcome, including
higher rates of breast conserving surgery, without an increase in long term
distant recurrence [@Derks_2018]. We have already shown that there are distinct
translational differences on treatment that can be used to differentiate
responsive and non-responsive primary breast cancer in neoadjuvant chemotherapy
[@Bownes_2019] in two modestly sized cohorts.  Additionally, this work hinted
at the possibility of these samples being of increased value to existing
prognostic tests, how ever this required a larger sample size to validate.
Additionally *Turnbull et. al 2015* previously showed that in a similar fashion
samples from neo-adjuvantly treated endocrine therapy patients could be used to
create novel biomarkers for the prediction of response to AI's
[@Turnbull_2015]. These findings suggest that the fold changes seen
on-treatment in the neo-adjuvant setting for sequentially sampled breast cancer
may be of inherent value for the treatment and management of breast cancer. 

Neoadjuvant trials are however comparatively scarce and there are additional
factors to consider for these samples including the increased cost for multiple
biopsies and the increased patient stress associated. It is possible to justify
these risks with significant increases to risk stratification and prognosis,
but for a sufficiently powered cohort cross-dataset integration is required.
This study proposes to trial integration techniques for patient matched
sequential samples of primary breast cancer to ascertain the viability of cross
study combination that will retain both the composition and biological
variation of the pre and post treatment samples, but that will also leave the
per patient transcriptional delta values intact. **  

Methods for the integration of continuous microarray expression data include
SVA[@Leek_2007], BFRM[@Carvalho_2008], FA[@Wang_2011] and AGC[@Kilpinen_2008].
These methods all seek the shift the distributions of expression towards one
another, and thus achieve integration. *ComBat* [@Johnson_2006] has been
established as the standard method for batch correction and integration of
microarray data [@M_ller_2016][@Chen_2011][@Kupfer_2012], and will be utilised
in this study as the primary tool for integration. *ComBat* uses an empirical
Bayesian method using combinations of additive and mulitplicative terms to
calculate the batch effect, and remove them from the gene expression data. This
technology does however rely on the assumption that the expression data is in
some way affected by these factors [@Johnson_2006]. We will discuss the steps
required to integrate the disparate datasets, and the decisions made to stop
this from being a problem of combinatorics, there were ten possible covariates
for analysis (Dataset, platform, biopsy time, Treatment type, response, tumour
grade, ER, PR and HER2 status and calculated intrinsic subtype), resulting in
3,628,800 (`10!`) total possible factor combinations.

Of the 10 possible factors; dataset, sequencing platform, type of treatment,
time on treatment, intrinsic subtype, known response, histological grade, ER
positivity, PR positivity and HER2 status the factors that were brought forward
for consideration were sequencing platform, treatment type and intrinsic
subtype. The reasoning for this reduction was that these factors inherently
encompassed the most information about each study, and were the most complete
annotation available. This would hopefully account for most of the
inter-dataset variation, and keep as much data as possible. This reduction was
made to reduce the number of possible combinations from ~3M (`10!`) to 24 (`4!`), and was
based on the following rational. These factors were the most complete features,
and would thus result in the largest retention of samples. Sequencing platform
is a known technical variable for batch correction of microarray data and also
encompassed the trial of origin which encapsulated various other features by
proxy, including environmental, procedural, and technical variables that would
be hospital/institute specific as well as all having the same inclusion
criteria for the associated trial.  Treatment type, as one possible important
metric would be whether the disease was treated with chemotherapy or endocrine
therapy as ER+ low grade tumours treated with AI alone will be translationally
different to ER+ high grade, and ER-/PR- tumours treated with chemotherapy.
Because we have previously shown that on-treatment effects of neoadjuvant
therapy have significant impact to the translational landscape of breast
cancer, the feature of on-treatment time was kept to try and maintain the delta
values between treatment times, which underpin the value of these samples.
Lastly calculated intrinsic subtype, these are scored off subsets of highly
researched genes, with known important biological functions, we have chosen to
integrate on these in an attempt to minimize the changes to the underlying
biological state of each sample in as much as each class can explain. 


We will also discuss the scoring metrics for the comparison of *Combat*
with various relevant covariates to establish the performance of each model.
Lastly, we present the results of this work as a new, publicly available tool
for validation testing and exploratory analysis, as well as our findings from
this data. 

## Materials

This study is comprised of five data sets of either neoadjuvantly chemotherapy
treated patients with matched pre-treatment, on-treatment, and surgical biopsy
samples, or aromatase inhibitor treated patients with similarly matching
samples. These are all sourced from publicly available data (GSE59515,
GSE20181, GSE87411, GSE122630, GSE32603), details of the total study
size, treatment architecture, biopsy schedule, and platform are contained below
in table 1. There is a lot of similarity between the treatments of the
neoadjuvantly treated AI cohorts, with many of the samples being treated with
Letrozol. Dissimilarity of the two chemotherapy cohorts is reflective of
underlying clinical differences in the trials, the I-SPY trial (C2) saw
patients of a generally worse prognosis. There is an imbalance of the study
design between the five cohorts, this is reflective of the number of matched
paired samples, and the number of possible matches, see Table 2. Comparisons of
time points is only made cross-study where direct comparisons are made. 

#### Table 1

Data-Set | GEO | Sample Size | Study Structure | Treatment | Platform
---|---|---|---|---|--- 
E1 | GSE59515 |  385 | Pre-treatment, Two weeks, Three Months, Longer Term Samples | Anastrozole & Letrozole | GPL10558 & GPL96  
E2 | GSE20181 | 176 | Pre-treatment, 10-14 days, 90 days | Letrozol | GPL96 
E3 | GSE87411 |  218 | Pre-treatment, 2-4 weeks | anastrozole, exemestane, letrozole | G4112F 
C1 | GSE122630 | 93 | Pre-treatment, two weeks, mid chemo, surgical biopsy | FEC, Docetaxel | Ion Ampliseq
C2 | GSE32603 | 248 | Pre-treatment, 24-72 hours, surgical biopsy | anthracycline, followed by taxane | HAQQLAB_Human_40986
---|---|---|---|---|--- 


Additionally, the numbers of matched pairs for each study are tabulated in Table 1. Additional
annotation information for the aggregate data can be found in supplementary
file 1. This includes the full annotation for the combined cohorts. This
represents 33 features in its entirety. 



#### Table 2 

Data-Set | Pre-Treatment | Early On-Treatment | Late On-Treatment | Surgical Sample | Total 
---|---|---|---|---|--- 
E1 | 131 | 110 | X | 132 | 373 
E2 | 58 | 58 | X | 60 | 176 
E3 | 109 | 97 | 12 | X | 218 
C1 | 34 | 12 | 24 | 25 | 95 
C2 | 141 | 45 | X | 62 | 248 
---|---|---|---|---|--- 
Total | 473 | 322 | 36 | 279 | 1110


## Methods

Collection of the data was done with GEOquery [@geo], with the exception of
DS1 which is compromised of two partially overlapping studies of Illumina and
Affy data, the contents, description of that work is detailed by *Kitched et.
al* [@Kitchen_2011]. Preprocessing and
normalization of the data was handled in R [@R], with the standard library and
Bioconductor [@BIO] packages Limma [@Limma] and edgeR [@edge1] [@edge2],
subtyping and risk assessment were performed by the Genefu package [@genfu].
Unsupervised cluster analysis was performed with base R `prcomp` and all
visualization was handled by ggplot2 [@ggplot]. Data manipulation and cleaning
was performed primarily within tidyverse [@tidy], data packaging and function
wrapping was done using using base R [@R] and Biobase [@BIO], as well as XDE
[@XDE] for custom S4 data types, and oxygen2 [@oxy] for documentation. 

Each dataset was initially cleaned an preprocessed individually, read counts
were converted to counts per million and filtered for genes with expression in
atleast half the samples to avoid inclusion of genes with no recorded
expression level data (NAs). Library counts were summed for each sample to
ensure that each passed a per-dataset threshold and was not significantly
larger or smaller than it's contemporaries. Datasets were then all voom
normalized to normalize the datasets on the same features.

Matching annotation files were curated and formatted by combining all of the
available clinical annotation and phenotypic information for as many factors as
possible. Initial subtypes and risk scores for IC10 [@IC101][@IC102][@IC103],
Pam50 [@IC103], scmod1 [@desmedt2008biological] and scmod2 [@wirapati2008meta],
ssp2003 [@IC101] and ssp2006 [@IC102] , MammaPrint [@van_t_Veer_2002] and rorS
[@IC103] continual and categorical risk values were calculated and appended to
each annotation data frame. These were then separately packaged as S4
ExpressionSets for integration. 

Naive approaches to dataset integration were first performed with inner
(retaining only the expression features common to all data sets) and full
(retaining all features across all datasets) joins of data, with added
interpolation of missing features to preserve dimensionality (i.e. keep full
gene annotation) across datasets and quantile normalization was used to retain
the ranking of gene expression on a per dataset basis, while converging the
expression distributions. Principal component analysis (PCA) and t-Distributed
Stochastic Neighbor Embedding (T-SNE) were used to visualize the effect of
integration on the pre and post integration data, subtypes and risk scores were
recalculated post combination and concordance values between the subtypes and
risks were plotted on a per patient level. 

*ComBat* was utilized for integration using various combinations of
covariates. Purrr [@purrr] was used to functionalize the testing
process and perform the integration on all possible combinations of integration
covariates. Concordance scores for each test were calculated for each sample
at each time point and compared to the reference scores/subtypes calculated
independently prior to integration. PCA plots were made to visualize the
difference in variance space and the linear combination of factors that make up
PC1 and PC2 were compared on a per-dataset basis to make quantitative
comparisons between the same samples pre and post integration. Randomforest
models were trained to identify batch in the post integration data (80:20
training/test split) and used to predict the batch on untrained data as another
metric of integration success.

Ultimately, an ExpressionSetList object was created with uniformly normalized
expression data, and combined outer joined annotation information to create the
most feature complete dataset. Following assimiliation meta analysis of the
relative calculated subtype and prognosis scores was performed, and log delta
values of the matched pre and on-treatment samples were clustered to identify
uniformity in expression changes across trials and treatment types. The
complete R code and full visualization of the results are hosted at
`https://rjbownes.gitlab.io/Integration_Rationale`. Lastly, the combined
datasets are now available as an R package, currently hosed at
`https:://gitlab.com/rjbownes/MetaMatchedBreast`.

## Results

Baseline results for composition 

	1. Composition of each data set in terms of each subtyping tests.
	2. median continuous risk score, and table of categorical risk score for each data set. 
	3. Expression distribution.
	4. PCA of unitegrated results. 
	5. PC1 PC2 linear products list and overlap
    6. Differentially expressed gene lists

### Naive Integration Results

Simple merging of the datasets was not a serious attempt at dataset
integration, but served as an important reference upon which all metrics could
improve. This methodology takes into consideration no features or variables
with technical or biological relevance to the study, and is purely to establish
a base line.

#### Inner and Outer Joins 

	1. Resultant dimensions from each join.
		- Number of genes full
		- number of genes inner
	2. ability to perform genefu tests
		- can perform tests on full
		- can not perform on inner
	3. PCA of resultant integration
		- full 
		- inner
	4. Comparison of pre/post int PC1 PC2 make up. 
	5. concordance values for each test:
		- aggregate performace 
	6. RF results
	7. Correlation pre/post
    8. DE gene list overlap

### ComBat Integration with covariates

*ComBat* with the 

#### Platform 

	1. PCA of resultant integration
		- PC1 PC2 overlap per dataset pre/post
	2. concordance values for each test:
		- aggregate performace 
	3. RF results
	4. Composite score

#### Treatment Type

	1. PCA of resultant integration
		- PC1 PC2 overlap per dataset pre/post
	2. concordance values for each test:
		- aggregate performace 
	3. RF results
    4. Correlation pre/post
    5. DE gene list overlap


#### Treatment Status
	
	1. PCA of resultant integration
		- PC1 PC2 overlap per dataset pre/post
	2. concordance values for each test:
		- aggregate performace 
	3. RF results
    4. Correlation pre/post
    5. DE gene list overlap


#### Intrinsic Subtype 

	1. PCA of resultant integration
		- PC1 PC2 overlap per dataset pre/post
	2. concordance values for each test:
		- aggregate performace 
	3. RF results
    4. Correlation pre/post
    5. DE gene list overlap

#### Results from MetaMatchedBreast

	1. Table of completeness for the annotation dataframe.
	2. overlapping PCA plot with scale variance space
	3. Change of subtype over time across NA therapy.
	4. Change in Risk score over time with NA.
	5. unsupervised clustering of delta values 
	6. Kaplan meier plots of subtype assignment at each time point. 

## Discussion 

	1. Plot of composite score for each MAJOR integration category, with sub
categories. 
	2. Discuss the sweeping changes presented by each integration technique.
	3. underlying biology changed
	4. discordance too high for serious consideration.
	5. What are the aggregrate changes seen on-treatment:
		- Subtype
		- risk score
	6. How do the delta values cluster?
	7. how do the survival curves change on treatment? 

## Conclusion

Integration of sequentially matched neoadjuvantly treated datasets is a
formidable obstacle to large scale on-treatment breast cancer analysis.
Presented in this paper is a viable alternaitve and the first of its kind
attempt at packaging this data for public access and reproducibilty. Results
from the analysis of this combined data revealed X, Y and Z.   
