--- 
title: "Integration and Alternatives"
author: "Richard Bownes"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
description: "Collecting and integrating disparate data sets."
---
```{r, include=FALSE}
library(tidyverse)
library(knitr)
```
# Forward

Integration of disparate on-treatment chemo and endocrine therapy is a valuable
target in breast cancer research. Increasing the frequency of low occurrence
events and minority subgroups helps to better understand and build models off
rarer samples, and allows for the cross treatment examination of expression
level changes. A fundamental understanding of the differences, or lack of
differences present in different types of therapy is lacking in the corpus of
research from a transcriptional and bioinformatics approach. This study seeks
to identify the best method to integrate heterogeneous data into larger, more
powerful cohorts to conduct this work. I will **NOT** be taking every possible
approach to integration that is possible, there are far too many alternatives
and far too little time. I will outline a few prominent confounding factors
that hinders the normal integration of this type of data, variables including
the processing platform, treatment status of the sample, treatment type and the
intrinsic subtype composition of the data sets. For each of these factors I
will present a method for rationally combining the data and evaluating if the
end result has damaged the underlying biological information, and a summary on
the results. Lastly, I will offer an alternative solution should these all
prove nonviable and a description of what this means for research going
forward. 

## Accessing the Data {-}

We have five data sets to contend with containing the valuable matched
pre/on-treatment samples, 3 endocrine therapy, 2 chemo therapy. Links to the
publicly 

### Endocrine Therapy {-}

- Data set 1 (E1)
  - [GSE59515](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE59515)
    - 385 Total Samples
    - Pre-Treatment, two weeks on treatment, three months and longer term samples.
    - Patients treated with aromatase inhibitors (anastrozole, letrozol)
    - Combined Illumina HT-12v4 BeadChips (GPL10558) and Affymetrix U133A GeneChips (GPL96)
- Data set 2 (E2)
  - [GSE20181](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE20181)
    - 176 Total Samples
    - Pre-Treatment, 10-14 days on treatment, and 90 days on treatment. 
    - Aromatase inhibitors (letrozol)
    - Affymetrix Human Genome U133A Array (GPL96)
- Data set 3 (E3)
  - [GSE87411](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE87411)
    - 218 Total Samples
    - Pre-Treatment and 2-4 weeks on treatment.
    - Treatment with anastrozole, exemestane, or letrozole. None responsive patients triaged to NAC.
    - Agilent-014850 Whole Human Genome Microarray 4x44K G4112F 

### Chemotherapy {-}

- Data set 4 (C1)
  - [GSE122630](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE122630) 
    - 93 Samples
    - Pre-Treatment, two weeks, mid chemo and surgical samples.
    - Patients were primarily treated with three cycles of FEC and Docetaxel with Herceptin where appropriate. Three patients received Paclitaxel, one patient received additional Carboplatin, one patient received Epi-Cyclophosphamide and Paclitaxel, and one patient received Docetaxel and Cyclophosphamide.
    - Ion Ampliseq
- Data set 5 (C2)
  - [GSE32603](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE32603)
    - 248 Total Samples
    - Pre-Treatment, 24-72 hours post treatment and resection samples.
    - Treated with anthracycline, followed by taxane prior to surgery.  
    - UCSF/HAQQLAB_Human_40986_ISPY
  
## Summary Statistics {-}

The data sets contain Pre-Treatment Samples (PT), Surgical Samples (TS) and
either one or two on-treatment samples (T-On, early on-treatment, TM,
mid-treatment). The following table highlights the numbers at each time points
per dataset and their sums.

Data-Set | Pre-Treatment | Early On-Treatment | Late On-Treatment | Surgical Sample | Total 
---|---|---|---|---|---
E1 | 131 | 110 | X | 132 | 373
E2 | 58 | 58 | X | 60 | 176
E3 | 109 | 97 | 12 | X | 218
C1 | 34 | 12 | 24 | 25 | 95
C2 | 141 | 45 | X | 62 | 248
---|---|---|---|---|---
Total | 473 | 322 | 36 | 279 | 1110

The raw annotation for the five datasets are very divergent in terms of
labelling and handling of categorical variables. I have spent a lot of time
handling all of the variations and combining them/filtering them into one
cohesive annotation file. 


```{r}
read.csv("data/Annotation/annotationST.csv", header = TRUE, row.names = "Row.names") %>%
  dplyr::select(-c("X.1")) %>% 
  as_tibble() %>% 
  arrange(X) %>% 
  head()
```

